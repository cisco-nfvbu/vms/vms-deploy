#!/usr/bin/env python

from functools import wraps
import time

import utils

__author__ = "mhalinge"
__email__ = "mhalinge@cisco.com"


class ContextDecorator(object):
    def __init__(self, **kwargs):
        self.__dict__.update(kwargs)

    def __enter__(self):
        return self

    def __exit__(self, typ, val, traceback):
        pass

    def __call__(self, f):
        @wraps(f)
        def wrapper(*args, **kw):
            with self:
                return f(*args, **kw)
        return wrapper


class log_runtime(ContextDecorator):
    def __enter__(self):
        print "************** {}: {} **************".format(self.label, self.description)
        self.start_time = time.time()
        return self

    def __exit__(self, typ, val, traceback):
        time_taken = time.time() - self.start_time
        self.phases.append({
            "name": self.label,
            "description": self.description,
            "time_taken": utils.time_taken_prettify(time_taken)
        })
        print "************** {}: {} completed in {}**************\n"\
            .format(self.label, self.description, utils.time_taken_prettify(time_taken))
