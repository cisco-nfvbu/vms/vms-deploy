#!/usr/bin/env python

import os
import subprocess
import urllib2
import json
import time

__author__ = "mhalinge"
__email__ = "mhalinge@cisco.com"


class ProcessUtils:
    def __init__(self, process):
        self.process = process
        self.DEVNULL = open(os.devnull, 'wb')

    def is_running(self, pid_file):
        if not os.path.exists(pid_file):
            return None
        pid = open(pid_file, 'r').read()
        if pid == '':
            return None
        pid = int(pid)

        return self.kill(pid, 0)

    def kill(self, pid, sig=0):
        try:
            os.kill(pid, sig)
            return pid
        except OSError:
            return None

    @staticmethod
    def is_binary_in_path(binary):
        try:
            subprocess.check_output(["which", binary])
        except subprocess.CalledProcessError:
            raise ValueError(binary + ' not found in the path')

    def is_registered_on_consul(self, service):
        resp = ProcessUtils.call_url("http://localhost:8500/v1/health/service/{}".format(service))
        if len(resp) == 0:
            return False
        else:
            for item in resp:
                for check in item["Checks"]:
                    if "ServiceName" in check and "Status" in check\
                            and check["ServiceName"] == service and check["Status"] == "passing":
                        return True
        return False

    def stop_this_pid(self, pid_file):
        pid = self.is_running(pid_file)
        if pid is None:
            print "{} isn't running".format(self.process)
            if os.path.exists(pid_file):
                os.remove(pid_file)
            return
        self.kill(pid, 9)
        print "{} kill initiated".format(self.process)
        time.sleep(5)

        end_time = time.time() + 60
        while self.is_running(pid_file) is not None:
            if time.time() > end_time:
                print "The stubborn process refuses to die! Process: {} Pid: {}" \
                    .format(self.process, self.is_running(pid_file))
                return
            print "Trying to kill this stubborn process again. Process: {} Pid: {}" \
                .format(self.process, self.is_running(pid_file))
            if self.is_running(pid_file) is not None:
                self.kill(int(self.is_running(pid_file)), 9)
                time.sleep(5)
        if os.path.exists(pid_file):
            os.remove(pid_file)
        print "{} finally killed".format(self.process)

    @staticmethod
    def call_url(url):
        return json.loads(urllib2.urlopen(url).read())

