import os
import shutil

from distutils.spawn import find_executable

__author__ = "mhalinge"
__email__ = "mhalinge@cisco.com"


def time_taken_prettify(time_taken):
    hour = int(time_taken // (60 * 60))
    minute = int((time_taken - (hour * 60 * 60)) // 60)
    seconds = time_taken - (hour * 60 * 60) - (minute * 60)

    time_taken_message = ""
    if hour != 0:
        time_taken_message += ("{} hour " if hour == 1 else "{} hours ").format(hour)
    if minute != 0:
        time_taken_message += ("{} minute " if minute == 1 else "{} minutes ").format(minute)
    if seconds != 0:
        time_taken_message += ("{} second " if minute == 1 else "{} seconds ").format(seconds)
    return time_taken_message


def read_property_file(property_file_path):
    return dict(l.rstrip().split('=') for l in open(property_file_path)
                if not (l.startswith("#") or len(l.rstrip().split('=')) == 1))


def remove_dirs(directory_path):
    if os.path.exists(directory_path):
        os.chmod(directory_path, 0777)
        shutil.rmtree(directory_path)
        print "{} deleted!".format(directory_path)
    else:
        print "{} does not exist!!".format(directory_path)


def make_exec(binary):
    os.chmod(find_executable(binary), 0775)

