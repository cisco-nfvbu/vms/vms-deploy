#!/usr/bin/env bash

git config --global credential.helper 'cache --timeout 7200'
git config --global url."https://".insteadOf git://

pip install tabulate
pip install pyyaml

npm install -g log.io

gem install --source http://api.rubygems.org mailcatcher #smtp://127.0.0.1:1025

chmod +x ./**/*.py
chmod +x ./**/**/*.py

source ./ccsrc.sh