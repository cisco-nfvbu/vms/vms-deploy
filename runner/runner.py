#!/usr/bin/env python

import os
import sys
import time
from tabulate import tabulate

from tools.git import GIT
from tools.mvn import MVN

from requisites.runner import Runner as Requisites_Runner
from microservices.runner import Runner as Micro_Services_Runner

from requisites.cassandra import Cassandra
from requisites.mail_catcher import MailCatcher
from requisites.log_io import LogIO

from ui.platform import PlatformUI
from ui.usecase import UseCaseUI

import utils
from utils.process_utils import ProcessUtils
from utils.context_decorator import log_runtime


__author__ = "mhalinge"
__email__ = "mhalinge@cisco.com"


def use_case_runner(use_case, ms_list, branch=None, populate=None, mode=None):
    use_case_repo = GIT(os.getenv("REPO_" + str(use_case).upper()))
    use_case_ui = UseCaseUI(use_case_repo.directory)
    if branch is not None:
        with log_runtime(label=use_case + "_repo",
                         description="Switching to branch {} for {}".format(branch, use_case),
                         phases=phases):
            use_case_repo.shift_to(branch)

        with log_runtime(label=use_case + "_repo",
                         description="Pulling latest code for {}".format(use_case),
                         phases=phases):
            use_case_repo.pull()

    use_case_ms_runner = Micro_Services_Runner(use_case, ms_list)

    if use_case_ms_runner is not None:
        if do_build:
            with log_runtime(label="builder",
                             description="Building things for {}".format(use_case),
                             phases=phases):
                use_case_ms_runner.build()

        with log_runtime(label=use_case + "_runner",
                         description="Stopping {} micro services if already running".format(use_case),
                         phases=phases):
            use_case_ms_runner.stop()
        if populate is not None and should_populate:
            with log_runtime(label=use_case + "_runner",
                             description="Populating {} micro services: {}, "
                                         "mode: {}, parallel?: {}".format(use_case, populate, mode, parallel_populate),
                             phases=phases):
                use_case_ms_runner.populate(option=populate, mode=mode, parallel=parallel_populate)

        with log_runtime(label=use_case + "_runner",
                         description="Starting {} micro services".format(use_case),
                         phases=phases):
            use_case_ms_runner.start()
            time.sleep(10)

        with log_runtime(label=use_case + "_runner",
                         description="Checking if the {} micro services have registered with Consul".format(use_case),
                         phases=phases):
            use_case_ms_runner.is_registered_on_consul()

        with log_runtime(label=use_case + "_ui",
                         description="Building and dispatching {} ui".format(use_case),
                         phases=phases):
            use_case_ui.build()


if __name__ == "__main__":
    start_time = time.time()
    nfvusvcssupport_repo = GIT(os.getenv("REPO_NFVUC"))
    vmsMicroserviceFramework_repo = GIT(os.getenv("REPO_VMS"))

    arguments = sys.argv

    phases = []

    requisites_runner = Requisites_Runner()
    platform_ui = PlatformUI("UI")
    mail_catcher = MailCatcher()
    log_io = LogIO()

    artifact_runner = None
    infra_runner = None
    platform_runner = None

    use_cases = []

    branches = [None] * 7
    commits = [None] * 7
    populate = ["database", "all"]
    mode = ["NewInstall"] * 2

    kill_switch = False
    do_build = True
    restart_requisites = False
    clean_requisites = False
    should_run_with_use_case = False
    should_populate = False
    parallel_populate = False
    drop_keyspaces = False
    fake_smtp = False

    builder = MVN()
    arguments.append('dummy')

    if "kill" in arguments:
        pid_file_list = [f for f in os.listdir(os.getenv("PATH_PID_VMS"))
                         if not os.path.isfile(f) and f.endswith(".pid")]
        for pid_file in pid_file_list:
            process_utils = ProcessUtils(pid_file.split('.pid')[0])
            process_utils.stop_this_pid(os.path.join(os.getenv("PATH_PID_VMS"), pid_file))
        exit(0)

    for i in range(len(arguments) - 1):
        argument = arguments[i + 1]

        if argument.split(':')[0] == 'branch' and len(argument.split(':')) > 1:
            branch_list = argument.split(':')[1].split(',')

            if len(branch_list) == 1:
                branch_list = branch_list * 7
            for j in range(len(branch_list)):
                if branch_list[j] != "":
                    branches[j] = branch_list[j]
            del branch_list

        if argument.split(':')[0] == 'commit' and len(argument.split(':')) > 1:
            commit_list = argument.split(':')[1].split(',')

            for j in range(len(commit_list)):
                commits[j] = commit_list[j]
            del commit_list

        if argument.split(':')[0] == 'populate':
            should_populate = True
            if len(argument.split(':')) == 1:
                argument += ":database"

            populate_list = argument.split(':')[1].split(',')

            if len(populate_list) == 1:
                populate_list = populate_list * 2
            for j in range(len(populate_list)):
                if populate_list[j] == '':
                    populate[j] = None
                else:
                    populate[j] = populate_list[j]
            del populate_list

        if argument.split(':')[0] == 'mode':
            if len(argument.split(':')) == 1:
                argument += ":NewInstall"

            mode_list = argument.split(':')[1].split(',')

            if len(mode_list) == 1:
                mode_list = mode_list * 2
            for j in range(len(mode_list)):
                if mode_list[j] != '':
                    mode[j] = mode_list[j]
            del mode_list

        if argument.split(':')[0] == 'use_case'\
                or argument.split(':')[0] == 'use-case' \
                or argument.split(':')[0] == 'usecase' \
                or argument.split(':')[0] == 'uc':
            should_run_with_use_case = True
            if len(argument.split(':')) == 1:
                argument += ":cvpn,vb"
            use_case_list = argument.split(':')[-1].split(',')
            for use_case in use_case_list:
                use_cases.append(use_case.lower())

            del use_case_list

        if argument == '-no-build' \
                or argument == '-nobuild' \
                or argument == '-nb':
            do_build = False

        if argument == '-parallel' \
                or argument == '-pp':
            parallel_populate = True

        if argument.split(':')[0] == '-restart_requisites' \
                or argument.split(':')[0] == '-restartrequisites' \
                or argument.split(':')[0] == '-restart-requisites' \
                or argument.split(':')[0] == '-rr':
            restart_requisites = True
            options = argument.split(':')[-1].split(',')
            for option in options:
                clean_requisites = option == 'clean_requisites' \
                                   or option == 'cleanrequisites' \
                                   or option == 'clean-requisites' \
                                   or option == 'cr'

        if argument == '-drop-keyspaces' \
                or argument == '-drk' \
                or argument == '-dk':
            drop_keyspaces = True

        if argument == '-smtp' \
                or argument == '-fake_smtp':
            fake_smtp = True

    if restart_requisites:
        with log_runtime(label="requisites_runner",
                         description="Stop Requisites",
                         phases=phases):
            requisites_runner.stop()
        if clean_requisites:
            with log_runtime(label="requisites_runner",
                             description="Clean Requisites data",
                             phases=phases):
                requisites_runner.clean()
        with log_runtime(label="requisites_runner",
                         description="Starting Requisites",
                         phases=phases):
            requisites_runner.start()
            time.sleep(30)
    with log_runtime(label="requisites_runner",
                     description="Check status for the requisites",
                     phases=phases):
        requisites_runner.status()

    if drop_keyspaces:
        with log_runtime(label="cassandra",
                         description="Drop keyspaces",
                         phases=phases):
            cassandra = Cassandra()
            should_populate = True
            populate = ["database", "all"]
            mode = ["NewInstall"] * 2

            for keyspace in cassandra.keyspaces():
                print "Dropping {}".format(keyspace)
                cassandra.drop_keyspace(keyspace)

    if fake_smtp:
        with log_runtime(label="mail_catcher",
                         description="Restart fake smtp server: mail_catcher",
                         phases=phases):
            mail_catcher.restart()
            mail_catcher.status()

    if branches[0] is not None:
        with log_runtime(label="nfvusvcssupport_repo",
                         description="Switching to branch {} for NFV Support".format(branches[0]),
                         phases=phases):
            nfvusvcssupport_repo.shift_to(branches[0])

        with log_runtime(label="nfvusvcssupport_repo",
                         description="Pulling latest code for NFV Support",
                         phases=phases):
            nfvusvcssupport_repo.pull()

    if branches[1] is not None:
        with log_runtime(label="vmsMicroserviceFramework_repo",
                         description="Switching to branch {} for VMS".format(branches[1]),
                         phases=phases):
            vmsMicroserviceFramework_repo.shift_to(branches[1])

        with log_runtime(label="vmsMicroserviceFramework_repo",
                         description="Pulling latest code for VMS",
                         phases=phases):
            vmsMicroserviceFramework_repo.pull()
    if do_build:
        with log_runtime(label="builder",
                         description="Building things for NFV Support",
                         phases=phases):
            builder.clean(nfvusvcssupport_repo.directory)
            builder.install(nfvusvcssupport_repo.directory)
        with log_runtime(label="builder",
                         description="Building things for VMS",
                         phases=phases):
            builder.clean(vmsMicroserviceFramework_repo.directory)
            builder.install(vmsMicroserviceFramework_repo.directory)
        with log_runtime(label="platform_ui",
                         description="Building things for VMS UI",
                         phases=phases):
            platform_ui.install()

    infra_runner = Micro_Services_Runner("infra", [
        "routerservice",
        # "phispheremanager"
    ])
    platform_runner = Micro_Services_Runner("platform", [
        "administrationservice",
        "billingservice",
        "consumeservice",
        "devicemanagerservice",
        "manageservice",
        "monitorservice",
        "notificationservice",
        "orchestrationservice",
        "serviceextensionservice",
        "usermanagementservice"
    ])

    if infra_runner is not None:
        with log_runtime(label="infra_runner",
                         description="Restarting Infra micro services",
                         phases=phases):
            infra_runner.restart()
            time.sleep(10)
        with log_runtime(label="infra_runner",
                         description="Checking if the Infra micro services have registered with Consul",
                         phases=phases):
            infra_runner.is_registered_on_consul(timeout=5)

    if platform_runner is not None:
        with log_runtime(label="platform_runner",
                         description="Stopping VMS micro services if already running",
                         phases=phases):
            platform_runner.stop()
        if populate[0] is not None and should_populate:
            with log_runtime(label="platform_runner",
                             description="Populating VMS micro services: {}, "
                                         "mode: {}, parallel?: {}".format(populate[0], mode[0], parallel_populate),
                             phases=phases):
                platform_runner.populate(option=populate[0], mode=mode[0], parallel=parallel_populate)

        with log_runtime(label="platform_runner",
                         description="Starting VMS micro services",
                         phases=phases):
            platform_runner.start()
            time.sleep(10)

        with log_runtime(label="platform_runner",
                         description="Checking if the VMS micro services have registered with Consul",
                         phases=phases):
            platform_runner.is_registered_on_consul(timeout=25)

        with log_runtime(label="platform_ui",
                         description="Restarting Platform UI",
                         phases=phases):
            platform_ui.build()
            platform_ui.restart()

    if should_run_with_use_case and ("cloudvpn" in use_cases or "cvpn" in use_cases):
        use_case_runner(
            use_case="cloudvpn",
            ms_list=[
                "cloudvpn",
                # "firewall",
                #  "dhcp",
                #  "vce"
            ],
            branch=branches[2],
            populate=populate[1] if should_populate else None,
            mode=mode[1])

    if should_run_with_use_case and ("vb" in use_cases or "vbranch" in use_cases):
        use_case_runner(
            use_case="vbranch",
            ms_list=[
                "vbranch"
            ],
            branch=branches[3],
            populate=populate[1] if should_populate else None,
            mode=mode[1])

    if should_run_with_use_case and ("sdwan" in use_cases or "sd" in use_cases or "sd-wan" in use_cases
                                     or "sdwan-viptela" in use_cases):
        use_case_runner(
            use_case="sdwan",
            ms_list=[
                "sd-wan"
            ],
            branch=branches[4],
            populate=populate[1] if should_populate else None,
            mode=mode[1])

    if should_run_with_use_case and ("mr" in use_cases or "manageddevice" in use_cases or "managed-cpe" in use_cases):
        use_case_runner(
            use_case="managedcpe",
            ms_list=[
                "manageddevice"
            ],
            branch=branches[5],
            populate=populate[1] if should_populate else None,
            mode=mode[1])

    if should_run_with_use_case and ("iwan" in use_cases):
        use_case_runner(
            use_case="iwan",
            ms_list=[
                "iwan",
            ],
            branch=branches[6],
            populate=populate[1] if should_populate else None,
            mode=mode[1])

    with log_runtime(label="log_io",
                     description="Stream logs to UI",
                     phases=phases):
        log_io.restart()
        log_io.status()

    end_time = time.time()

    print tabulate(phases,
                   headers={"name": "Name", "description": "Process", "time_taken": "Time taken"},
                   # showindex="always",
                   floatfmt=".4f")

    print "It just took me {}to get everything functional* (*mostly)"\
        .format(utils.time_taken_prettify(end_time - start_time))

    print "======================="
    print "== That's all folks! =="
    print "======================="
