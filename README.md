# VMS Deployer

>  Work in Progress doc

### \_\_init\_\_()
`open ccsrc.sh` # update the relevant folder paths

`source ccsrc.sh` # always do this

### Get started

`python runner/runner.py usecase:{usecase} {options}`

###### Cases
1. `python runner/runner.py`
2. `python runner/runner.py -rr`
3. `python runner/runner.py -rr -nb`
4. `python runner/runner.py -rr -nb -dk`
5. `python runner/runner.py -rr -nb -dk -pp`
6. `python runner/runner.py populate:database -rr -nb -pp`
7. `python runner/runner.py usecase:cvpn`
8. `python runner/runner.py usecase:cvpn -rr`
9. `python runner/runner.py usecase:cvpn -rr -nb`
10. `python runner/runner.py usecase:cvpn -rr -nb -dk`
11. `python runner/runner.py usecase:cvpn -rr -nb -dk -pp` **# You will mostly need this one**
12. `python runner/runner.py usecase:cvpn populate:database,all -rr -nb -pp`
13. `python runner/runner.py usecase:cvpn populate:database,all mode:Upgrade -rr -nb -pp`
....
It can be whatever you want to be actually. Refer to the tables below to make your own

###### Usecases

Append: `usecase:cvpn,iwan...`


| key | Use case |
| --- | --- |
| cvpn | [CloudVPN](https://cto-github.cisco.com/NFV-BU/cloud-vpn) |
| sd | [SD-WAN](https://cto-github.cisco.com/NFV-BU/sdwan-viptela) |
| mr | [Managed CPE](https://cto-github.cisco.com/NFV-BU/managed-cpe) |
| vb | [vBranch](https://cto-github.cisco.com/NFV-BU/vbranch) |
| iwan | [IWAN](https://cto-github.cisco.com/NFV-BU/IWAN) |

###### Options
There can be at least **42** different combinations of below options you can use.

| Command | Option |
|---|---|
| -rr | Restart requisties |
| -nb | Do not build |
|  -dk | Drop cassandra keyspaces |
| -pp | Parallel data population |
| populate:database,all | `db` populate for platform + `all` populate for usecases |
| mode:NewInstall | populate mode |