#!/bin/bash

export PATH_SELF=`pwd`
export PYTHONPATH=${PYTHONPATH}:${PATH_SELF}

#Requisites
export PATH_CASSANDRA=~/Softwares/apache-cassandra-3.10
export PATH_REDIS=~/Softwares/redis-3.2.0
export PATH_KAFKA=~/Softwares/kafka_2.10-0.10.2.0
export PATH_ELASTIC_SEARCH=~/Softwares/elasticsearch-5.6.8
export PATH_CONSUL=~/Softwares
export PATH_VAULT=~/Softwares

export PATH=${PATH}:${PATH_CASSANDRA}/bin:${PATH_REDIS}/src:${PATH_ELASTIC_SEARCH}/bin:${PATH_CONSUL}:${PATH_VAULT}

#Repos
export REPO_NFVUC=~/_git_base/nfvusvcssupport
export REPO_VMS=~/_git_base/VMSMicroserviceFramework
export REPO_IWAN=~/_git_base/IWAN
export REPO_CLOUDVPN=~/_git_base/cloud-vpn
export REPO_VBRANCH=~/_git_base/vbranch
export REPO_SDWAN=~/_git_base/sdwan-viptela
export REPO_MANAGEDCPE=~/_git_base/managed-cpe

#Data
export DATA_CONSUL=/usr/local/var/dev/phi/consul

#Configurations
export CONF_REDIS=~/Softwares/redis-3.2.0/redis.conf
export CONF_CONSUL=/etc/consul.d

#PID_Folder
export PATH_PID_VMS=~/vms_deploy_pid

#Logs_Folder
export PATH_LOGS_VMS=~/vms_deploy_logs

mkdir -p ${PATH_PID_VMS}
mkdir -p ${PATH_LOGS_VMS}

source vms_stuff.sh

echo ">>>>>>>>>>>>>>>>>>  WIP   <<<<<<<<<<<<<<<<<<"
echo "Usage: runner/runner.py branch:develop,develop,develop,develop populate:database,database mode:NewInstall,Upgrade use_case:iwan,cloudvpn"
