#!/usr/bin/env python

import subprocess
import os
import sys

import utils
from requisites.requisite import Requisites

__author__ = "mhalinge"
__email__ = "mhalinge@cisco.com"


class Kafka(Requisites):
    def __init__(self, pid_file_path=os.getenv("PATH_PID_VMS"), log_file_path=os.getenv("PATH_LOGS_VMS")):
        Requisites.__init__(self, str(self.__class__).split('.')[-1], pid_file_path, log_file_path)

    def start(self):
        pid = self.process_util.is_running(self.pid_file)
        if pid is not None:
            print self.process + " already running at " + str(pid)
        else:
            pid = subprocess.Popen([
                os.path.join(os.getenv("PATH_KAFKA"), "bin", "kafka-server-start.sh"),
                os.path.join(os.getenv("PATH_KAFKA"), "config", "server.properties")
            ], stdout=self.process_util.DEVNULL, stderr=subprocess.STDOUT).pid
            pidfile = open(self.pid_file, 'w')
            pidfile.write(str(pid))
            pidfile.close()
            print "{} started at {}".format(self.process, self.process_util.is_running(self.pid_file))

    def clean(self):
        properties = utils.read_property_file(os.path.join(os.getenv("PATH_KAFKA"), "config", "server.properties"))
        if "log.dirs" in properties and os.path.exists(properties["log.dirs"]):
            utils.remove_dirs(properties["log.dirs"])
        else:
            print "It's clean already"


if __name__ == "__main__":
    kafka = Kafka()
    options = {
        'start': kafka.start,
        'stop': kafka.stop,
        'restart': kafka.restart,
        'status': kafka.status,
        'clean': kafka.clean
    }

    arguments = sys.argv

    if len(arguments) == 1:
        arguments = [
            'restart',
            'status'
        ]

    for item in arguments:
        if item in options:
            options[item]()
