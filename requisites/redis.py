#!/usr/bin/env python

import subprocess
import os
import sys

from requisites.requisite import Requisites

__author__ = "mhalinge"
__email__ = "mhalinge@cisco.com"


class Redis(Requisites):
    def __init__(self, pid_file_path=os.getenv("PATH_PID_VMS"), log_file_path=os.getenv("PATH_LOGS_VMS")):
        Requisites.__init__(self, str(self.__class__).split('.')[-1], pid_file_path, log_file_path)

        self.binary = "redis-server"
        self.process_util.is_binary_in_path(self.binary)

    def start(self):
        pid = self.process_util.is_running(self.pid_file)
        if pid is not None:
            print self.process + " already running at " + str(pid)
        else:
            pid = subprocess.Popen([
                self.binary,
                os.getenv("CONF_REDIS")
            ], stdout=self.process_util.DEVNULL, stderr=subprocess.STDOUT).pid
            pidfile = open(self.pid_file, 'w')
            pidfile.write(str(pid))
            pidfile.close()
            print "{} started at {}".format(self.process, self.process_util.is_running(self.pid_file))


if __name__ == "__main__":
    redis = Redis()
    options = {
        'start': redis.start,
        'stop': redis.stop,
        'restart': redis.restart,
        'status': redis.status
    }

    arguments = sys.argv

    if len(arguments) == 1:
        arguments = [
            'restart',
            'status'
        ]

    for item in arguments:
        if item in options:
            options[item]()
