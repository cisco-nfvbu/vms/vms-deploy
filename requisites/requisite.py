#!/usr/bin/env python

import time
import os

from utils.process_utils import ProcessUtils

__author__ = "mhalinge"
__email__ = "mhalinge@cisco.com"


class Requisites:
    def __init__(self, process, pid_file_path, log_file_path):
        self.process = process.lower()
        self.process_util = ProcessUtils(self.process)
        self.pid_file = os.path.join(pid_file_path, "requisite_" + self.process + ".pid")
        self.log_file_path = log_file_path
        self.log_file_path = os.path.join(self.log_file_path, "requisite_" + process + ".log")

    def start(self):
        pass

    def stop(self):
        self.process_util.stop_this_pid(self.pid_file)

    def restart(self):
        print self.process + " restarting"

        self.stop()
        time.sleep(2)
        self.start()

    def status(self):
        pid = self.process_util.is_running(self.pid_file)
        if pid is not None:
            print self.process + " running at " + str(pid)
        else:
            print self.process + " not running"
        return pid

    def clean(self):
        print "Implement this"

