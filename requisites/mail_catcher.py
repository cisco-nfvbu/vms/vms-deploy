#!/usr/bin/env python

import os
import subprocess
import sys

from requisites.requisite import Requisites

__author__ = "mhalinge"
__email__ = "mhalinge@cisco.com"


class MailCatcher(Requisites):
    def __init__(self, pid_file_path=os.getenv("PATH_PID_VMS"), log_file_path=os.getenv("PATH_LOGS_VMS")):
        Requisites.__init__(self, str(self.__class__).split('.')[-1], pid_file_path, log_file_path)

        self.binary = "mailcatcher"
        self.process_util.is_binary_in_path(self.binary)

        self.ip = os.getenv("IP_MACHINE") if os.getenv("IP_MACHINE") else "localhost"
        self.port = "1025"
        self.http_port = "5319"

    def start(self):
        pid = self.process_util.is_running(self.pid_file)
        if pid is not None:
            print self.process + " already running at " + str(pid)
        else:
            command = [
                self.binary,
                "--no-quit",
                "-f",
                "--ip", self.ip,
                "--smtp-port", self.port,
                "--http-port", self.http_port
            ]
            pid = subprocess.Popen(command,
                                   stdout=self.process_util.DEVNULL, stderr=subprocess.STDOUT).pid
            pidfile = open(self.pid_file, 'w')
            pidfile.write(str(pid))
            pidfile.close()
            print "{} started at {}".format(self.process, self.process_util.is_running(self.pid_file))


if __name__ == "__main__":
    mail_catcher = MailCatcher()
    options = {
        'start': mail_catcher.start,
        'stop': mail_catcher.stop,
        'restart': mail_catcher.restart,
        'status': mail_catcher.status
    }

    arguments = sys.argv

    if len(arguments) == 1:
        arguments = [
            'restart',
            'status'
        ]

    for item in arguments:
        if item in options:
            options[item]()
