#!/usr/bin/env python

import os
import subprocess
import sys
import yaml
import json

from requisites.requisite import Requisites

__author__ = "mhalinge"
__email__ = "mhalinge@cisco.com"


class LogIOServer(Requisites):
    def __init__(self, pid_file_path=os.getenv("PATH_PID_VMS"), log_file_path=os.getenv("PATH_LOGS_VMS")):
        Requisites.__init__(self, str(self.__class__).split('.')[-1], pid_file_path, log_file_path)

        self.binary = "log.io-server"
        self.process_util.is_binary_in_path(self.binary)

        self.directory_conf = os.path.expanduser(os.path.join("~", ".log.io"))
        self.directory_conf_harvester = os.path.join(self.directory_conf, "harvester.conf")
        self.definition_log_files = os.path.join(os.getenv("PATH_SELF", '.'), "log_files.json")

    def start(self):
        pid = self.process_util.is_running(self.pid_file)
        if pid is not None:
            print self.process + " already running at " + str(pid)
        else:
            pid = subprocess.Popen([
                self.binary
            ], stdout=self.process_util.DEVNULL, stderr=subprocess.STDOUT).pid
            pidfile = open(self.pid_file, 'w')
            pidfile.write(str(pid))
            pidfile.close()

            log_streams = json.load(open(self.definition_log_files, 'r'))

            for service in log_streams:
                for i in range(0, len(log_streams[service])):
                    log_streams[service][i] = os.path.expandvars(log_streams[service][i])

            content = open(self.directory_conf_harvester, 'r').read()
            start = content.split("=")[0]
            content = yaml.load(content.split("=")[1].replace("\t", "").replace("\n", ""))
            content["nodeName"] = "vms"
            content["logStreams"] = log_streams

            conf_file = open(self.directory_conf_harvester, 'w')
            conf_file.write(start + "= " + json.dumps(content, sort_keys=True, indent=4))
            conf_file.close()

            print "{} started at {}".format(self.process, self.process_util.is_running(self.pid_file))


class LogIOHarvester(Requisites):
    def __init__(self, pid_file_path=os.getenv("PATH_PID_VMS"), log_file_path=os.getenv("PATH_LOGS_VMS")):
        Requisites.__init__(self, str(self.__class__).split('.')[-1], pid_file_path, log_file_path)

        self.binary = "log.io-harvester"
        self.process_util.is_binary_in_path(self.binary)

    def start(self):
        pid = self.process_util.is_running(self.pid_file)
        if pid is not None:
            print self.process + " already running at " + str(pid)
        else:
            pid = subprocess.Popen([
                self.binary
            ], stdout=self.process_util.DEVNULL, stderr=subprocess.STDOUT).pid
            pidfile = open(self.pid_file, 'w')
            pidfile.write(str(pid))
            pidfile.close()
            print "{} started at {}".format(self.process, self.process_util.is_running(self.pid_file))


class LogIO:
    def __init__(self):
        self.log_IO_server = LogIOServer()
        self.log_IO_harvester = LogIOHarvester()

    def start(self):
        self.log_IO_server.start()
        self.log_IO_harvester.start()

    def stop(self):
        self.log_IO_server.stop()
        self.log_IO_harvester.stop()

    def restart(self):
        self.log_IO_server.restart()
        self.log_IO_harvester.restart()

    def status(self):
        self.log_IO_server.status()
        self.log_IO_harvester.status()


if __name__ == "__main__":
    log_io = LogIO()

    options = {
        'start': log_io.start,
        'stop': log_io.stop,
        'restart': log_io.restart,
        'status': log_io.status
    }

    arguments = sys.argv

    for item in arguments:
        if item in options:
            options[item]()
