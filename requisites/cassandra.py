#!/usr/bin/env python

import subprocess
import os
import time
import sys

import utils
from requisites.requisite import Requisites

__author__ = "mhalinge"
__email__ = "mhalinge@cisco.com"


class Cassandra(Requisites):
    def __init__(self, pid_file_path=os.getenv("PATH_PID_VMS"), log_file_path=os.getenv("PATH_LOGS_VMS")):
        Requisites.__init__(self, str(self.__class__).split('.')[-1], pid_file_path, log_file_path)

        self.binary = "cassandra"
        self.process_util.is_binary_in_path(self.binary)

    def start(self, allow_root=False):
        pid = self.process_util.is_running(self.pid_file)
        if pid is not None:
            print self.process + " already running at " + str(pid)
        else:
            command = [
                self.binary,
                "-p", self.pid_file
            ]
            if allow_root:
                command.append("-R")

            subprocess.call(command,
                            stdout=self.process_util.DEVNULL,
                            stderr=subprocess.STDOUT
                            )
            time.sleep(2)
            print "{} started at {}".format(self.process, self.process_util.is_running(self.pid_file))

    def keyspaces(self):
        command = [
            "cqlsh",
            "-e", "desc keyspaces"
        ]
        proc = subprocess.Popen(command, stdout=subprocess.PIPE)
        keyspace_list = []
        for line in proc.stdout:
            keyspace_list.extend(line.rsplit())

        keyspace_list = [keyspace for keyspace in keyspace_list
                         if keyspace.startswith("vms_") or keyspace.startswith("skyfall_")]
        return keyspace_list

    def drop_keyspace(self, keyspace):
        command = [
            "cqlsh",
            "-e", "drop keyspace {}".format(keyspace)
        ]
        subprocess.Popen(command)

    def clean(self):
        if os.path.exists(os.path.join(os.getenv("PATH_CASSANDRA"), 'data', 'commitlog')):
            utils.remove_dirs(os.path.join(os.getenv("PATH_CASSANDRA"), 'data', 'commitlog'))
        else:
            print "It's clean already"


if __name__ == "__main__":
    cassandra = Cassandra()
    options = {
        'start': cassandra.start,
        'stop': cassandra.stop,
        'restart': cassandra.restart,
        'status': cassandra.status,
        'drop_keyspace': cassandra.drop_keyspace,
        'keyspaces': cassandra.keyspaces
    }

    arguments = sys.argv

    if len(arguments) == 1:
        arguments = [
            'restart',
            'status'
        ]

    for item in arguments:
        if item in options:
            options[item]()


