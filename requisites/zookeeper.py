#!/usr/bin/env python

import subprocess
import os
import sys

import utils
from requisites.requisite import Requisites

__author__ = "mhalinge"
__email__ = "mhalinge@cisco.com"


class Zookeeper(Requisites):
    def __init__(self, pid_file_path=os.getenv("PATH_PID_VMS"), log_file_path=os.getenv("PATH_LOGS_VMS")):
        Requisites.__init__(self, str(self.__class__).split('.')[-1], pid_file_path, log_file_path)

    def start(self):
        pid = self.process_util.is_running(self.pid_file)
        if pid is not None:
            print self.process + " already running at " + str(pid)
        else:
            pid = subprocess.Popen([
                os.path.join(os.getenv("PATH_KAFKA"), "bin", "zookeeper-server-start.sh"),
                os.path.join(os.getenv("PATH_KAFKA"), "config", "zookeeper.properties")
            ], stdout=self.process_util.DEVNULL, stderr=subprocess.STDOUT).pid
            pidfile = open(self.pid_file, 'w')
            pidfile.write(str(pid))
            pidfile.close()
            print "{} started at {}".format(self.process, self.process_util.is_running(self.pid_file))

    def clean(self):
        properties = utils.read_property_file(os.path.join(os.getenv("PATH_KAFKA"), "config", "zookeeper.properties"))
        if "dataDir" in properties and os.path.exists(properties["dataDir"]):
            utils.remove_dirs(properties["dataDir"])
        else:
            print "It's clean already"


if __name__ == "__main__":
    zookeeper = Zookeeper()
    options = {
        'start': zookeeper.start,
        'stop': zookeeper.stop,
        'restart': zookeeper.restart,
        'status': zookeeper.status,
        'clean': zookeeper.clean
    }

    arguments = sys.argv

    if len(arguments) == 1:
        arguments = [
            'restart',
            'status'
        ]

    for item in arguments:
        if item in options:
            options[item]()
