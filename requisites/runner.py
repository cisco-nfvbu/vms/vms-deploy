#!/usr/bin/env python

import time
import sys

from requisites.cassandra import Cassandra
from requisites.redis import Redis
from requisites.zookeeper import Zookeeper
from requisites.kafka import Kafka
from requisites.vault import Vault
from requisites.elasticsearch import ElasticSearch
from requisites.consul import Consul

__author__ = "mhalinge"
__email__ = "mhalinge@cisco.com"


class Runner:
    def __init__(self):
        self.cassandra = Cassandra()
        self.redis = Redis()
        self.zookeeper = Zookeeper()
        self.kafka = Kafka()
        self.vault = Vault()
        self.elasticsearch = ElasticSearch()
        self.consul = Consul()

    def start(self):
        self.cassandra.start()
        self.redis.start()
        self.zookeeper.start()
        self.vault.start()
        self.elasticsearch.start()
        time.sleep(10)
        self.kafka.start()
        time.sleep(4)
        self.consul.start()

    def stop(self):
        self.cassandra.stop()
        self.redis.stop()
        self.zookeeper.stop()
        self.vault.stop()
        self.elasticsearch.stop()
        self.kafka.stop()
        self.consul.stop()

    def restart(self):
        self.stop()
        time.sleep(5)
        self.start()
        time.sleep(5)
        self.status()

    def status(self):
        self.cassandra.status()
        self.redis.status()
        self.zookeeper.status()
        self.kafka.status()
        self.vault.status()
        self.elasticsearch.status()
        self.consul.status()

    def clean(self):
        self.cassandra.clean()
        self.zookeeper.clean()
        self.kafka.clean()
        self.consul.clean()


if __name__ == "__main__":
    runner = Runner()

    options = {
        'start': runner.start,
        'stop': runner.stop,
        'restart': runner.restart,
        'status': runner.status,
        'clean': runner.clean
    }

    arguments = sys.argv

    if len(arguments) == 1:
        arguments = [
            'restart',
            'status'
        ]

    for item in arguments:
        if item in options:
            options[item]()

