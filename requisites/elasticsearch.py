#!/usr/bin/env python

import subprocess
import os
import time
import sys
import utils

from requisites.requisite import Requisites

__author__ = "mhalinge"
__email__ = "mhalinge@cisco.com"


class ElasticSearch(Requisites):
    def __init__(self, pid_file_path=os.getenv("PATH_PID_VMS"), log_file_path=os.getenv("PATH_LOGS_VMS")):
        Requisites.__init__(self, str(self.__class__).split('.')[-1], pid_file_path, log_file_path)

        self.binary = "elasticsearch"
        self.process_util.is_binary_in_path(self.binary)

    def start(self):
        pid = self.process_util.is_running(self.pid_file)
        if pid is not None:
            print self.process + " already running at " + str(pid)
        else:
            subprocess.Popen([
                self.binary,
                "-p" + self.pid_file,
            ], stdout=self.process_util.DEVNULL, stderr=subprocess.STDOUT)
            time.sleep(10)
            print "{} started at {}".format(self.process, self.process_util.is_running(self.pid_file))

    def clean(self):
        if os.path.exists(os.path.join(os.getenv("PATH_CASSANDRA"), 'data', 'commitlog')):
            utils.remove_dirs(os.path.join(os.getenv("PATH_CASSANDRA"), 'data', 'commitlog'))
        else:
            print "It's clean already"

    def get_cluster_details(self):
        if self.process_util.is_running(self.pid_file) is not None:
            return self.process_util.call_url("http://localhost:9200")
        return None


if __name__ == "__main__":
    elastic_search = ElasticSearch()
    options = {
        'start': elastic_search.start,
        'stop': elastic_search.stop,
        'restart': elastic_search.restart,
        'status': elastic_search.status,
        'get_cluster_details': elastic_search.get_cluster_details
    }

    arguments = sys.argv

    if len(arguments) == 1:
        arguments = [
            'restart',
            'status'
        ]

    for item in arguments:
        if item in options:
            options[item]()
