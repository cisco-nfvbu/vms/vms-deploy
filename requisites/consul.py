#!/usr/bin/env python

import subprocess
import os
import time
import sys

import utils
from requisites.requisite import Requisites

__author__ = "mhalinge"
__email__ = "mhalinge@cisco.com"


class Consul(Requisites):
    def __init__(self, pid_file_path=os.getenv("PATH_PID_VMS"), log_file_path=os.getenv("PATH_LOGS_VMS")):
        Requisites.__init__(self, str(self.__class__).split('.')[-1], pid_file_path, log_file_path)

        self.binary = "consul"
        self.process_util.is_binary_in_path(self.binary)

        self.conf_d = os.getenv("CONF_CONSUL")
        self.data_d = os.getenv("DATA_CONSUL")

    def start(self):
        pid = self.process_util.is_running(self.pid_file)
        if pid is not None:
            print self.process + " already running at " + str(pid)
        else:
            subprocess.Popen([
                self.binary,
                "agent",
                "-server",
                "-ui",
                "-client=127.0.0.1",
                "-bind=127.0.0.1",
                "-bootstrap-expect=1",
                "-pid-file=" + self.pid_file,
                "-data-dir=" + self.data_d,
                "-config-dir=" + self.conf_d
            ], stdout=self.process_util.DEVNULL, stderr=subprocess.STDOUT)
            time.sleep(10)
            print "{} started at {}".format(self.process, self.process_util.is_running(self.pid_file))

    def clean(self):
        utils.remove_dirs(self.data_d)


if __name__ == "__main__":
    consul = Consul()
    options = {
        'start': consul.start,
        'stop': consul.stop,
        'restart': consul.restart,
        'status': consul.status
    }

    arguments = sys.argv

    if len(arguments) == 1:
        arguments = [
            'restart',
            'status'
        ]

    for item in arguments:
        if item in options:
            options[item]()
