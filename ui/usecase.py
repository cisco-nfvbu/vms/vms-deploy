#!/usr/bin/env python

from distutils.dir_util import copy_tree
import os
import sys

from tools.node import Gulp
from tools.node import NPM
from tools.node import Yarn

__author__ = "mhalinge"
__email__ = "mhalinge@cisco.com"


class UseCaseUI:
    def __init__(self, directory):
        self.do_gulp = os.path.exists(os.path.join(directory, "SIF", "ui"))
        if self.do_gulp:
            self.directory = os.path.join(directory, "SIF", "ui")
        else:
            self.directory = os.path.join(directory, "ui")

        self.gulp = Gulp()
        self.npm = NPM()
        self.yarn = Yarn()

    def build(self):
        self.npm.install(self.directory)
        if self.do_gulp:
            self.npm.install_package(self.directory, "gulp")
            self.gulp.build(self.directory)
        else:
            self.npm.install_package(self.directory, "yarn")
            self.yarn.install(self.directory)
            self.npm.run(self.directory, ["build"])
        copy_tree(os.path.join(self.directory, "build"),
                  os.path.join(os.getenv("REPO_VMS"), "nfvui", "client", "build", "services"))


if __name__ == "__main__":
    arguments = sys.argv

    directory = os.path.abspath('.')
    for i in range(len(arguments) - 1):
        argument = arguments[i + 1]

        if argument.split(':')[0] == 'dir' and len(argument.split(':')) > 1:
            directory = argument.split(':')[1]

        if argument.split(':')[0] == 'ms'\
                or argument.split(':')[0] == 'uc'\
                and len(argument.split(':')) > 1:
            directory = os.getenv("REPO_" + str(argument.split(':')[1]).upper())

    useCaseUI = UseCaseUI(directory)
    useCaseUI.build()
