#!/usr/bin/env python

import time
import sys
import os

from utils.process_utils import ProcessUtils
from tools.node import Node
from tools.node import Gulp
from tools.node import NPM
from tools.node import Bower

__author__ = "mhalinge"
__email__ = "mhalinge@cisco.com"


class PlatformUI:
    def __init__(self, process, pid_file_path=os.getenv("PATH_PID_VMS")):
        self.process = process
        self.process_util = ProcessUtils(self.process)
        self.pid_file = os.path.join(pid_file_path, "ui_" + process + ".pid")
        self.directory = os.path.join(os.getenv("REPO_VMS"), "nfvui")

        self.node = Node()
        self.gulp = Gulp()
        self.npm = NPM()
        self.bower = Bower()

    def start(self):
        pid = self.process_util.is_running(self.pid_file)
        if pid is not None:
            print self.process + " already running at " + str(pid)
        else:
            pid = self.node.run(self.directory, "server/server.js")

            pidfile = open(self.pid_file, 'w')
            pidfile.write(str(pid))
            pidfile.close()
            print self.process + " started at " + str(pid)

    def install(self):
        self.npm.install(self.directory)
        self.npm.install_package(self.directory, "bower")
        self.npm.install_package(self.directory, "gulp")

        self.bower.install(self.directory)

    def clean(self):
        self.gulp.clean(self.directory)

    def build(self):
        self.gulp.build(self.directory)

        g_config_file = open(os.path.join(self.directory, "client", "build", "gconfig.js"), 'a')
        g_config_file.write("\nGATEWAY_URL = '{}';".format(os.getenv("GATEWAY_URL")))
        g_config_file.write("\nUSERMANAGEMENTURL = GATEWAY_URL + '{}';".format("/idm"))
        g_config_file.write("\nGOOGLE_API_KEY = '{}';".format(os.getenv("GOOGLE_API_KEY")))
        g_config_file.close()

    def watcher(self):
        self.gulp.run(self.directory, ["watch:all"])

    def stop(self):
        self.process_util.stop_this_pid(self.pid_file)

    def restart(self):
        print self.process + " restarting"

        self.stop()
        time.sleep(2)
        self.build()
        time.sleep(2)
        self.start()
        time.sleep(4)
        self.status()

    def status(self):
        pid = self.process_util.is_running(self.pid_file)
        if pid is not None:
            print self.process + " running at " + str(pid)
        else:
            print self.process + " not running"


if __name__ == "__main__":
    platformUI = PlatformUI("UI")

    options = {
        'start': platformUI.start,
        'stop': platformUI.stop,
        'restart': platformUI.restart,
        'status': platformUI.status,
        'build': platformUI.build,
        'install': platformUI.install,
        'watcher': platformUI.watcher
    }

    arguments = sys.argv

    if len(arguments) == 1:
        arguments = [
            'restart',
            'watcher'
        ]

    for item in arguments:
        if item in options:
            options[item]()
