#!/usr/bin/env python

import subprocess
import os
import sys

from utils.process_utils import ProcessUtils

__author__ = "mhalinge"
__email__ = "mhalinge@cisco.com"


class MVN:
    def __init__(self, binary="mvn"):
        self.binary = binary
        ProcessUtils.is_binary_in_path(self.binary)

    def clean(self, directory):
        command = [
            self.binary,
            "clean"
        ]
        os.chdir(directory)
        subprocess.call(command)

    def install(self, directory, skip_tests=True, update_snapshot=True):
        command = [
            self.binary,
            "install"
        ]
        if skip_tests:
            command.append("-DskipTests=true")
            command.append("-Dmaven.test.skip=true")

        if update_snapshot:
            command.append("-U")

        os.chdir(directory)
        subprocess.call(command)


if __name__ == "__main__":
    directory = os.path.abspath('.')
    arguments = sys.argv

    for i in range(len(arguments) - 1):
        argument = arguments[i + 1]

        if argument.split(':')[0] == 'dir' and len(argument.split(':')) > 1:
            directory = argument.split(':')[1]

    mvn = MVN()
    mvn.install(directory)
