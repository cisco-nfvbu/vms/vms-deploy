#!/usr/bin/env python

import subprocess
import os
import time

from utils.process_utils import ProcessUtils

__author__ = "mhalinge"
__email__ = "mhalinge@cisco.com"


class NPM:
    def __init__(self, binary="npm"):
        self.binary = binary
        self.process_util = ProcessUtils("NPM")

    def install(self, directory):
        command = [
            self.binary,
            "install"
        ]

        os.chdir(directory)
        subprocess.call(command)

    def install_package(self, directory, package):
        command = [
            self.binary,
            "install",
            package
        ]

        os.chdir(directory)
        subprocess.call(command)

    def start(self, directory):
        os.chdir(directory)
        pid = subprocess.Popen([
                self.binary,
                "start"
            ], stdout=self.process_util.DEVNULL, stderr=subprocess.STDOUT).pid
        time.sleep(10)
        return pid

    def run(self, directory, command):
        os.chdir(directory)
        script = [
            self.binary,
            "run"
        ]
        if type(command) in (tuple, list):
            script.extend(command)
        else:
            script.append(command)
        pid = subprocess.Popen(script,
                               stdout=self.process_util.DEVNULL,
                               stderr=subprocess.STDOUT
                               ).pid
        time.sleep(10)
        return pid


class Node:
    def __init__(self, binary="node"):
        self.binary = binary
        self.process_util = ProcessUtils("Node")

    def run(self, directory, arguments):
        os.chdir(directory)

        command = list()
        command.append(self.binary)
        command.append(arguments)
        pid = subprocess.Popen(command, stdout=self.process_util.DEVNULL, stderr=subprocess.STDOUT).pid
        time.sleep(10)
        return pid


class Gulp:
    def __init__(self, binary="gulp"):
        self.binary = binary

    def clean(self, directory):
        command = [
            self.binary,
            "clean"
        ]

        os.chdir(directory)
        subprocess.call(command)

    def build(self, directory):
        command = [
            self.binary,
            "build"
        ]

        os.chdir(directory)
        subprocess.call(command)

    def run(self, directory, tasks):
        command = [
            self.binary
        ]
        command.extend(tasks)

        os.chdir(directory)
        subprocess.call(command)


class Bower:
    def __init__(self, binary="bower"):
        self.binary = binary

    def install(self, directory):
        command = [
            self.binary,
            "install"
        ]

        os.chdir(directory)
        subprocess.call(command)


class Yarn:
    def __init__(self, binary="yarn"):
        self.binary = binary

    def install(self, directory):
        command = [
            self.binary,
            "install"
        ]

        os.chdir(directory)
        subprocess.call(command)
