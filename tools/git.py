#!/usr/bin/env python

import os
import subprocess
import sys

__author__ = "mhalinge"
__email__ = "mhalinge@cisco.com"


class GIT:
    def __init__(self, directory, binary="git"):
        self.directory = directory
        self.binary = binary

    def pull(self):
        os.chdir(self.directory)
        subprocess.call([
            self.binary,
            "pull"
        ])

    def shift_to(self, branch="develop"):
        os.chdir(self.directory)
        subprocess.call([
            self.binary,
            "checkout",
            branch
        ])

if __name__ == "__main__":
    directory = os.path.abspath('.')
    arguments = sys.argv
    actions = []

    for i in range(len(arguments) - 1):
        argument = arguments[i + 1]

        if argument.split(':')[0] == 'dir' and len(argument.split(':')) > 1:
            directory = argument.split(':')[1]
        else:
            actions.append(argument)

    git_guy = GIT(directory)
    for action in actions:
        if action == "pull":
            git_guy.pull()

