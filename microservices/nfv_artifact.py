#!/usr/bin/env python

import os

from microservices.microservice import MicroService

__author__ = "mhalinge"
__email__ = "mhalinge@cisco.com"


class NFVArtifact(MicroService):
    def __init__(self, directory, micro_service, pid_file_path=os.path.expanduser("~"), log_file_path=os.getenv("PATH_LOGS_VMS")):
        MicroService.__init__(self, micro_service,
                              os.path.join(directory, micro_service + ".jar"),
                              pid_file_path, log_file_path)
