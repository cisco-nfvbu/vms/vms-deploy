#!/usr/bin/env python

import os
import sys
import time
import math

from microservices.nfv_infrastructureservices_repo import NFVInfrastructureServicesRepo
from microservices.nfv_applications_repo import NFVApplicationsRepo
from microservices.nfv_usecase_repo import NFVUseCaseRepo
from utils.threading_utils import Threader

from microservices.nfv_applications_repo.notificationservice import NotificationServiceMicroService
from microservices.nfv_applications_repo.monitorservice import MonitorServiceMicroService

__author__ = "mhalinge"
__email__ = "mhalinge@cisco.com"


class Runner:
    def __init__(self, project=None, ms_list=None):
        self.micro_services = {}
        for micro_service in ms_list:
            if project == "infra" or project == "nfv_infrastructure":
                self.micro_services[micro_service] = NFVInfrastructureServicesRepo(micro_service)
            elif project == "platform" or project == "nfv_applications":
                if micro_service == "notificationservice":
                    self.micro_services[micro_service] = NotificationServiceMicroService()
                elif micro_service == "monitorservice":
                    self.micro_services[micro_service] = MonitorServiceMicroService()
                else:
                    self.micro_services[micro_service] = NFVApplicationsRepo(micro_service)
            else:
                self.micro_services[micro_service] = NFVUseCaseRepo(os.getenv("REPO_" + str(project).upper()),
                                                                    micro_service)

    def build(self, micro_services=None):
        if micro_services is None:
            micro_services = self.micro_services
        for micro_service in sorted(micro_services):
            if micro_services[micro_service] is not None:
                micro_services[micro_service].build()
                time.sleep(10)

    def start(self, micro_services=None):
        if micro_services is None:
            micro_services = self.micro_services
        for micro_service in sorted(micro_services):
            if micro_services[micro_service] is not None:
                micro_services[micro_service].start()
                time.sleep(10)

    def stop(self, micro_services=None):
        if micro_services is None:
            micro_services = self.micro_services
        for micro_service in sorted(micro_services):
            if micro_services[micro_service] is not None:
                micro_services[micro_service].stop()

    def restart(self, micro_services=None):
        if micro_services is None:
            micro_services = self.micro_services
        self.stop(micro_services)
        self.start(micro_services)

    def status(self, micro_services=None):
        if micro_services is None:
            micro_services = self.micro_services
        for micro_service in sorted(micro_services):
            if micro_services[micro_service] is not None:
                micro_services[micro_service].status()

    def populate(self, micro_services=None, option="database", mode="NewInstall", parallel=False):
        population_threads_map = {}
        if micro_services is None:
            micro_services = self.micro_services
        if parallel:
            for micro_service in sorted(micro_services):
                if micro_services[micro_service] is not None:
                    def populate_function():
                        micro_services[micro_service].populate(option, mode if mode is not None else "NewInstall", True)
                    threader = Threader("{} populator".format(micro_service), populate_function)
                    threader.start()
                    population_threads_map[micro_service] = threader
                    time.sleep(15)

            populated = False

            while not populated:
                for micro_service in sorted(population_threads_map):
                    if population_threads_map[micro_service] is not None:
                        done = not population_threads_map[micro_service].isAlive()
                        if done:
                            population_threads_map.pop(micro_service)
                            print "{} populated*!".format(micro_service)
                            populated = len(population_threads_map) == 0
                if populated:
                    break
                time.sleep(100)
                print "Waiting for {} to complete data population" \
                    .format(", ".join(population_threads_map))
            for t in sorted(population_threads_map):
                population_threads_map[t].join()
        else:
            for micro_service in micro_services:
                if micro_services[micro_service] is not None:
                    micro_services[micro_service].populate(option, mode if mode is not None else "NewInstall")

    def is_registered_on_consul(self, micro_services=None, timeout=7):
        if micro_services is None:
            micro_services = self.micro_services

        end_time = time.time() + (timeout * 60)
        registered = False

        micro_service_list = dict(micro_services)

        while not registered:
            for micro_service in sorted(micro_service_list):
                if micro_service_list[micro_service] is not None:
                    done = micro_service_list[micro_service].is_registered_on_consul()
                    if done:
                        micro_service_list.pop(micro_service)
                        print "{} got registered with Consul finally!".format(micro_service)
                        registered = len(micro_service_list) == 0
            if time.time() > end_time:
                print "{} {} registered with Consul even after {} minutes of waiting!"\
                    .format(", ".join(micro_service_list),
                            "hasn't" if len(micro_service_list) == 1 else "haven't",
                            timeout)
                break
            if registered:
                break
            time.sleep(40)
            print "Will wait for another {} seconds for {} to get the Consul registration thing done"\
                .format(math.ceil(end_time - time.time()), ", ".join(micro_service_list))


if __name__ == "__main__":
    arguments = sys.argv

    project = "infra"
    list_of_ms = None
    options = []

    for i in range(len(arguments) - 1):
        argument = arguments[i + 1]
        if argument.split(':')[0] == 'project' and len(argument.split(':')) > 1:
            project = argument.split(':')[1]
        elif argument.split(':')[0] == 'list' and len(argument.split(':')) > 1:
            list_of_ms = list(set(argument.split(':')[1].split(',')))
        else:
            options.append(argument)

    if len(options) == 0:
        options = [
            'stop',
            'populate',
            'start'
        ]

    runner = Runner(project, list_of_ms)

    print "Micro Services: ", ', '.join(runner.micro_services.keys())
    print "Operations: ", ', '.join(options)

    for option in options:
        if option == 'start':
            runner.start(runner.micro_services)
        if option == 'stop':
            runner.stop(runner.micro_services)
        if option == 'restart':
            runner.restart(runner.micro_services)
        if option == 'status':
            runner.status(runner.micro_services)
        if option.split(':')[0] == 'populate':
            if len(option.split(':')) == 1:
                option += ':database'
            if len(option.split(':')) == 2:
                option += ':NewInstall'
            runner.populate(runner.micro_services, option.split(':')[1], option.split(':')[2])
        if option == 'on_consul':
            runner.is_registered_on_consul(runner.micro_services)

