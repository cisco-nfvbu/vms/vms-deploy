#!/usr/bin/env python

import os
import sys
import time

from tools.mvn import MVN
from microservices.microservice import MicroService

__author__ = "mhalinge"
__email__ = "mhalinge@cisco.com"


class NFVUseCaseRepo(MicroService):
    def __init__(self, directory, micro_service, pid_file_path=os.getenv("PATH_PID_VMS"), log_file_path=os.getenv("PATH_LOGS_VMS")):
        self.directory = directory
        self.directory_microservices = os.path.join(directory, "SIF", "microservice")

        if os.path.exists(os.path.join(self.directory_microservices, micro_service)):
            self.path = os.path.join(self.directory_microservices, micro_service)
        else:
            self.path = os.path.join(self.directory_microservices, micro_service + "service")

        MicroService.__init__(self, ''.join(micro_service.split('-')) + 'service',
                              os.path.join(self.path, "target", ''.join(micro_service.split('-')) + "service.jar"),
                              pid_file_path,
                              log_file_path)

    def build(self):
        mvn = MVN()
        mvn.clean(self.directory)
        mvn.install(self.directory)
        mvn.install(self.directory_microservices)
        mvn.install(self.path)


if __name__ == "__main__":
    arguments = sys.argv

    uc = "cloudvpn"
    ms = "cloudvpn"
    options = []

    for i in range(len(arguments) - 1):
        argument = arguments[i + 1]
        if argument.split(':')[0] == 'ms' and len(argument.split(':')) > 1:
            ms = argument.split(':')[1]
        elif argument.split(':')[0] == 'uc' and len(argument.split(':')) > 1:
            uc = argument.split(':')[1]
        else:
            options.append(argument)

    if len(options) == 0:
        options = [
            'stop',
            'populate',
            'start'
        ]

    nfvUseCaseRepo = NFVUseCaseRepo(os.getenv("REPO_" + str(uc).upper()), ms)

    for option in options:
        if option == 'build':
            nfvUseCaseRepo.build()
        if option.split(':')[0] == 'start':
            debug = None
            if len(option.split(':')) > 1:
                debug = option.split(':')[1]
            nfvUseCaseRepo.start(debug)
        if option == 'stop':
            nfvUseCaseRepo.stop()
        if option == 'restart':
            nfvUseCaseRepo.restart()
        if option == 'status':
            nfvUseCaseRepo.status()
        if option.split(':')[0] == 'populate':
            if len(option.split(':')) == 1:
                option += ':all'
            if len(option.split(':')) == 2:
                option += ':NewInstall'
                nfvUseCaseRepo.populate(option.split(':')[1], option.split(':')[2])
        if option == 'on_consul':
            print nfvUseCaseRepo.is_registered_on_consul()
        if option.split(':')[0] == 'on_consul_watch':
            if len(option.split(':')) == 1:
                option += ':60'
            if len(option.split(':')) == 2:
                option += ':10'
            time.sleep(int(option.split(':')[2]))
            nfvUseCaseRepo.is_registered_on_consul_watcher(int(option.split(':')[1]))