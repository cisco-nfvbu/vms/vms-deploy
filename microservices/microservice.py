#!/usr/bin/env python

import time
import subprocess
import os
import math

from utils.process_utils import ProcessUtils

__author__ = "mhalinge"
__email__ = "mhalinge@cisco.com"


class MicroService:
    def __init__(self, process, jar_file, pid_file_path, log_file_path):
        self.process = process
        self.process_util = ProcessUtils(self.process)
        self.pid_file = os.path.join(pid_file_path, "microservice_" + process + ".pid")
        self.log_folder_path = log_file_path
        self.log_file_path = os.path.join(self.log_folder_path, "microservice_" + process + ".log")
        self.jar_file = jar_file

    def start(self, debug=None, command_options=None):
        pid = self.process_util.is_running(self.pid_file)
        if pid is not None:
            print self.process + " already running at " + str(pid)
        else:
            os.putenv("spring.redis.password", "password")
            command = [
                "java",
                "-Xms128M", "-Xmx128M",
                "-XX:+HeapDumpOnOutOfMemoryError",
                "-Dspring.cloud.consul.discovery.preferIpAddress=true",
                "-Dspring.cloud.consul.discovery.ipAddress=127.0.0.1"
            ]
            if debug is not None:
                command.append("-Xrunjdwp:server=y,transport=dt_socket,suspend=n,address={}".format(debug))

            if command_options is not None:
                if type(command_options) is str:
                    command.append(command_options)
                elif type(command_options) is list:
                    command.extend(command_options)

            command.extend([
                "-jar",
                self.jar_file
            ])
            command.append("> {} 2>&1".format(self.log_file_path))
            os.chdir(self.log_folder_path)
            pid = subprocess.Popen(command, stdout=self.process_util.DEVNULL, stderr=subprocess.STDOUT).pid
            pidfile = open(self.pid_file, 'w')
            pidfile.write(str(pid))
            pidfile.close()
            print self.process + " started at " + str(pid)

    def stop(self):
        self.process_util.stop_this_pid(self.pid_file)

    def restart(self):
        print self.process + " restarting"

        self.stop()
        time.sleep(2)
        self.start()
        self.status()

    def status(self):
        pid = self.process_util.is_running(self.pid_file)
        if pid is not None:
            print self.process + " running at " + str(pid)
        else:
            print self.process + " not running"
        return pid

    def populate(self, option="database", mode="NewInstall", parallel=False, source_version="3.4.0"):
        os.putenv("spring.redis.password", "password")
        command = [
            "java",
            "-Dpopulate=" + option,
            "-Dmode=" + mode
        ]

        if mode.lower() == "upgrade":
            command.append("-DsourceVersion=" + source_version)

        command.extend([
            "-Xms128M", "-Xmx128M",
            "-XX:+HeapDumpOnOutOfMemoryError",
            "-jar",
            self.jar_file])

        os.chdir(self.log_folder_path)
        if parallel:
            subprocess.Popen(command, stdout=self.process_util.DEVNULL, stderr=subprocess.STDOUT).wait()
        else:
            subprocess.Popen(command).wait()

    def is_registered_on_consul(self):
        return self.process_util.is_registered_on_consul(self.process)

    def is_registered_on_consul_watcher(self, timeout=60):
        end_time = time.time() + timeout
        done = False

        while not done:
            done = self.is_registered_on_consul()
            if done:
                print "{} got registered with Consul finally!".format(self.process)
                break
            time.sleep(10)
            print "Will wait for another {} seconds for {} to get the Consul registration thing done" \
                .format(math.ceil(end_time - time.time()), self.process)
            if time.time() > end_time:
                print "{} hasn't registered with Consul even after {} seconds of waiting!" \
                    .format(self.process, timeout)
                break

