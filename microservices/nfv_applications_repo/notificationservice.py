#!/usr/bin/env python

import os
import sys
import time

from requisites.mail_catcher import MailCatcher
from microservices.nfv_applications_repo import NFVApplicationsRepo

__author__ = "mhalinge"
__email__ = "mhalinge@cisco.com"


class NotificationServiceMicroService(NFVApplicationsRepo):
    def __init__(self, pid_file_path=os.getenv("PATH_PID_VMS"), log_file_path=os.getenv("PATH_LOGS_VMS")):
        NFVApplicationsRepo.__init__(self, "notificationservice",
                                     pid_file_path, log_file_path)
        self.mail_catcher = MailCatcher()

    def start(self, debug=None, command_options=None):
        if command_options is None:
            command_options = []
        command_options.append("-Dnotification.notificationTemplatePath=" + os.getenv("NOTIFICATION_TEMPLATES_PATH"))
        if self.mail_catcher.status():
            command_options.append("-Dmail.smtpServerHost=" + self.mail_catcher.ip)
            command_options.append("-Dmail.port=" + self.mail_catcher.port)

        NFVApplicationsRepo.start(self, debug, command_options)


if __name__ == "__main__":
    arguments = sys.argv

    options = []

    for i in range(len(arguments) - 1):
        argument = arguments[i + 1]
        if argument.split(':')[0] == 'ms' and len(argument.split(':')) > 1:
            ms = argument.split(':')[1]
        else:
            options.append(argument)

    if len(options) == 0:
        options = [
            'stop',
            'populate',
            'start'
        ]

    notificationServiceMicroService = NotificationServiceMicroService()

    for option in options:
        if option == 'clean':
            notificationServiceMicroService.clean()
        if option == 'build':
            notificationServiceMicroService.build()
        if option.split(':')[0] == 'start':
            debug = None
            if len(option.split(':')) > 1:
                debug = option.split(':')[1]
            notificationServiceMicroService.start(debug)
        if option == 'stop':
            notificationServiceMicroService.stop()
        if option == 'restart':
            notificationServiceMicroService.restart()
        if option == 'status':
            notificationServiceMicroService.status()
        if option.split(':')[0] == 'populate':
            if len(option.split(':')) == 1:
                option += ':database'
            if len(option.split(':')) == 2:
                option += ':NewInstall'
                notificationServiceMicroService.populate(option.split(':')[1], option.split(':')[2])
        if option == 'on_consul':
            print notificationServiceMicroService.is_registered_on_consul()
        if option.split(':')[0] == 'on_consul_watch':
            if len(option.split(':')) == 1:
                option += ':60'
            if len(option.split(':')) == 2:
                option += ':10'
            time.sleep(int(option.split(':')[2]))
            notificationServiceMicroService.is_registered_on_consul_watcher(int(option.split(':')[1]))
