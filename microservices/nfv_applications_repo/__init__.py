#!/usr/bin/env python

import os
import sys
import time

from tools.mvn import MVN
from microservices.microservice import MicroService

__author__ = "mhalinge"
__email__ = "mhalinge@cisco.com"


class NFVApplicationsRepo(MicroService):
    def __init__(self, micro_service, pid_file_path=os.getenv("PATH_PID_VMS"), log_file_path=os.getenv("PATH_LOGS_VMS")):
        self.path = os.path.join(os.getenv("REPO_VMS"), "nfvapplications", micro_service)
        MicroService.__init__(self, micro_service,
                              os.path.join(self.path, "target", micro_service + ".jar"),
                              pid_file_path,
                              log_file_path)

    def clean(self):
        mvn = MVN()
        mvn.clean(self.path)

    def build(self):
        mvn = MVN()
        mvn.install(self.path)


if __name__ == "__main__":
    arguments = sys.argv

    ms = "usermanagementservice"
    options = []

    for i in range(len(arguments) - 1):
        argument = arguments[i + 1]
        if argument.split(':')[0] == 'ms' and len(argument.split(':')) > 1:
            ms = argument.split(':')[1]
        else:
            options.append(argument)

    if len(options) == 0:
        options = [
            'stop',
            'populate',
            'start'
        ]

    nfvApplicationsRepo = NFVApplicationsRepo(ms)

    for option in options:
        if option == 'clean':
            nfvApplicationsRepo.clean()
        if option == 'build':
            nfvApplicationsRepo.build()
        if option.split(':')[0] == 'start':
            debug = None
            if len(option.split(':')) > 1:
                debug = option.split(':')[1]
            nfvApplicationsRepo.start(debug)
        if option == 'stop':
            nfvApplicationsRepo.stop()
        if option == 'restart':
            nfvApplicationsRepo.restart()
        if option == 'status':
            nfvApplicationsRepo.status()
        if option.split(':')[0] == 'populate':
            if len(option.split(':')) == 1:
                option += ':database'
            if len(option.split(':')) == 2:
                option += ':NewInstall'
            nfvApplicationsRepo.populate(option.split(':')[1], option.split(':')[2])
        if option == 'on_consul':
            print nfvApplicationsRepo.is_registered_on_consul()
        if option.split(':')[0] == 'on_consul_watch':
            if len(option.split(':')) == 1:
                option += ':60'
            if len(option.split(':')) == 2:
                option += ':10'
            time.sleep(int(option.split(':')[2]))
            nfvApplicationsRepo.is_registered_on_consul_watcher(int(option.split(':')[1]))
