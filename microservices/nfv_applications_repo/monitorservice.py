#!/usr/bin/env python

import os
import sys
import time

from requisites.elasticsearch import ElasticSearch
from microservices.nfv_applications_repo import NFVApplicationsRepo

__author__ = "mhalinge"
__email__ = "mhalinge@cisco.com"


class MonitorServiceMicroService(NFVApplicationsRepo):
    def __init__(self, pid_file_path=os.getenv("PATH_PID_VMS"), log_file_path=os.getenv("PATH_LOGS_VMS")):
        NFVApplicationsRepo.__init__(self, "monitorservice",
                                     pid_file_path, log_file_path)
        self.elasticsearch = ElasticSearch()

    def start(self, debug=None, command_options=None):
        if command_options is None:
            command_options = []
        if self.elasticsearch.status():
            command_options.append("-Delasticsearch.cluster-name="
                                   + self.elasticsearch.get_cluster_details()["cluster_name"])

        NFVApplicationsRepo.start(self, debug, command_options)


if __name__ == "__main__":
    arguments = sys.argv

    options = []

    for i in range(len(arguments) - 1):
        argument = arguments[i + 1]
        if argument.split(':')[0] == 'ms' and len(argument.split(':')) > 1:
            ms = argument.split(':')[1]
        else:
            options.append(argument)

    if len(options) == 0:
        options = [
            'stop',
            'populate',
            'start'
        ]

    monitorServiceMicroService = MonitorServiceMicroService()

    for option in options:
        if option == 'clean':
            monitorServiceMicroService.clean()
        if option == 'build':
            monitorServiceMicroService.build()
        if option.split(':')[0] == 'start':
            debug = None
            if len(option.split(':')) > 1:
                debug = option.split(':')[1]
            monitorServiceMicroService.start(debug)
        if option == 'stop':
            monitorServiceMicroService.stop()
        if option == 'restart':
            monitorServiceMicroService.restart()
        if option == 'status':
            monitorServiceMicroService.status()
        if option.split(':')[0] == 'populate':
            if len(option.split(':')) == 1:
                option += ':database'
            if len(option.split(':')) == 2:
                option += ':NewInstall'
                monitorServiceMicroService.populate(option.split(':')[1], option.split(':')[2])
        if option == 'on_consul':
            print monitorServiceMicroService.is_registered_on_consul()
        if option.split(':')[0] == 'on_consul_watch':
            if len(option.split(':')) == 1:
                option += ':60'
            if len(option.split(':')) == 2:
                option += ':10'
            time.sleep(int(option.split(':')[2]))
            monitorServiceMicroService.is_registered_on_consul_watcher(int(option.split(':')[1]))
