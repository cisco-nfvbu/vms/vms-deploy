#!/usr/bin/env python

import os
import sys

from microservices.microservice import MicroService

__author__ = "mhalinge"
__email__ = "mhalinge@cisco.com"


class NFVInfrastructureServicesRepo(MicroService):
    def __init__(self, micro_service, pid_file_path=os.getenv("PATH_PID_VMS"), log_file_path=os.getenv("PATH_LOGS_VMS")):
        MicroService.__init__(self, micro_service,
                              os.path.join(os.getenv("REPO_VMS"), "nfvinfrastructureservices",
                                           micro_service, "target",
                                           micro_service + ".jar"),
                              pid_file_path,
                              log_file_path)

    def populate(self, option="database", mode="NewInstall", parallel=False, source_version="3.4.0"):
        pass


if __name__ == "__main__":
    arguments = sys.argv

    ms = "routerservice"
    options = []

    for i in range(len(arguments) - 1):
        argument = arguments[i + 1]
        if argument.split(':')[0] == 'ms' and len(argument.split(':')) > 1:
            ms = argument.split(':')[1]
        else:
            options.append(argument)

    if len(options) == 0:
        options = [
            'stop',
            'populate',
            'start'
        ]

    nfvInfrastructureServicesRepo = NFVInfrastructureServicesRepo(ms)

    for option in options:
        if option.split(':')[0] == 'start':
            debug = None
            if len(option.split(':')) > 1:
                debug = option.split(':')[1]
            nfvInfrastructureServicesRepo.start()
        if option == 'stop':
            nfvInfrastructureServicesRepo.stop()
        if option == 'restart':
            nfvInfrastructureServicesRepo.restart()
        if option == 'status':
            nfvInfrastructureServicesRepo.status()
        if option == 'on_consul':
            nfvInfrastructureServicesRepo.is_registered_on_consul()
